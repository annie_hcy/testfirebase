//
//  ViewController.swift
//  testFirebase
//
//  Created by Cheuk Yan HO on 10/12/2018.
//  Copyright © 2018 Cheuk Yan HO. All rights reserved.
//

import UIKit
import Firebase

class SignUpViewController: UIViewController {

    @IBOutlet weak var emailTextfield: UITextField!
    @IBOutlet weak var passwordTextfield: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func signUpPressed(_ sender: Any) {
        Auth.auth().createUser(withEmail: emailTextfield.text!, password: passwordTextfield.text!){
            (user, error) in
            
            if error != nil{
                print(error!)
            }
            else{
                print("Registration successful!")
            }
        }
    }
    
}

